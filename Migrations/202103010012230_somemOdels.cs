namespace Entity_RSHIPs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class somemOdels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Files",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Folder_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Folders", t => t.Folder_Id)
                .Index(t => t.Folder_Id);
            
            CreateTable(
                "dbo.Folders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Parent_Id = c.Int(),
                        FileId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.FileId)
                .ForeignKey("dbo.Folders", t => t.Parent_Id)
                .Index(t => t.Parent_Id)
                .Index(t => t.FileId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Folders", "Parent_Id", "dbo.Folders");
            DropForeignKey("dbo.Files", "Folder_Id", "dbo.Folders");
            DropForeignKey("dbo.Folders", "FileId", "dbo.Files");
            DropIndex("dbo.Folders", new[] { "FileId" });
            DropIndex("dbo.Folders", new[] { "Parent_Id" });
            DropIndex("dbo.Files", new[] { "Folder_Id" });
            DropTable("dbo.Folders");
            DropTable("dbo.Files");
        }
    }
}
