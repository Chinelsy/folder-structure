﻿using System.Collections.Generic;

namespace Entity_RSHIPs.Models
{
    public class Faculty
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int  DeptId { get; set; }
        public ICollection<Dept> Depts { get; set; }

    }
}